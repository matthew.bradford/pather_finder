use rand::{Rng, SeedableRng};
use rand_xorshift::XorShiftRng;

pub mod table;

pub struct RNG {
    rng: XorShiftRng,
}

#[allow(dead_code)]
impl RNG {
    pub fn new() -> RNG {
        let rng: XorShiftRng = SeedableRng::from_entropy();
        RNG { rng }
    }

    pub fn seed(seed: u64) -> RNG {
        let rng: XorShiftRng = SeedableRng::seed_from_u64(seed);
        RNG { rng }
    }

    pub fn rand<T>(&mut self) -> T
    where
        rand::distributions::Standard: rand::distributions::Distribution<T>,
    {
        self.rng.gen::<T>()
    }

    pub fn gen_range<T>(&mut self, min: T, max: T) -> T
    where
        T: rand::distributions::uniform::SampleUniform,
    {
        self.rng.gen_range(min, max)
    }

    pub fn roll_dice_u(&mut self, n: usize, die_type: usize) -> usize {
        (0..n).map(|_| self.gen_range(1, die_type + 1)).sum()
    }

    pub fn roll_dice_i(&mut self, n: i32, die_type: i32) -> i32 {
        (0..n).map(|_| self.gen_range(1, die_type + 1)).sum()
    }
}
