#[derive(Copy, Clone, Debug, serde::Deserialize, PartialEq, serde::Serialize)]
pub enum TileType {
    DownStairs,
    Floor,
    Wall,
}
