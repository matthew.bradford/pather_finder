use std::cmp::{max, min};
use std::ops::Sub;

use rltk::{Algorithm2D, BaseMap, Console, Point, Rltk, RGB};
use specs::prelude::*;

use crate::entities::{player::Player, Viewshed};
use crate::random::RNG;
use crate::world::{rect::Rect, tile::TileType};

pub const MAPWIDTH: usize = 80;
pub const MAPHEIGHT: usize = 50;
pub const MAPCOUNT: usize = MAPWIDTH * MAPHEIGHT;

#[derive(Clone, Component, Debug, serde::Deserialize, serde::Serialize)]
pub struct Map {
    pub blocked: Vec<bool>,
    pub depth: usize,
    pub revealed_tiles: Vec<bool>,
    pub rooms: Vec<Rect>,
    pub tiles: Vec<TileType>,
    pub visible_tiles: Vec<bool>,

    pub height: usize,
    pub height_i: i32,
    pub width: usize,
    pub width_i: i32,

    #[serde(skip_serializing)]
    #[serde(skip_deserializing)]
    pub content: Vec<Vec<Entity>>,
}

impl Default for Map {
    fn default() -> Map {
        Map {
            blocked: vec![false; MAPCOUNT],
            depth: 0,
            revealed_tiles: vec![false; MAPCOUNT],
            rooms: Vec::new(),
            tiles: vec![TileType::Wall; MAPCOUNT],
            visible_tiles: vec![false; MAPCOUNT],
            height: MAPHEIGHT,
            height_i: MAPHEIGHT as i32,
            width: MAPWIDTH,
            width_i: MAPWIDTH as i32,
            content: vec![Vec::new(); MAPCOUNT],
        }
    }
}

#[allow(dead_code)]
impl Map {
    fn apply_horizontal_tunnel(&mut self, x1: usize, x2: usize, y: usize) {
        for x in min(x1, x2)..=max(x1, x2) {
            let idx = self.u_xy_idx(x, y);
            if idx > 0 && idx < self.width as usize * self.height as usize {
                self.tiles[idx as usize] = TileType::Floor;
            }
        }
    }
    fn apply_room_to_map(&mut self, room: &Rect) {
        for y in room.y1 + 1..=room.y2 {
            for x in room.x1 + 1..=room.x2 {
                let idx = self.u_xy_idx(x, y);
                self.tiles[idx] = TileType::Floor;
            }
        }
    }
    fn apply_vertical_tunnel(&mut self, y1: usize, y2: usize, x: usize) {
        for y in min(y1, y2)..=max(y1, y2) {
            let idx = self.u_xy_idx(x, y);
            if idx > 0 && idx < self.width as usize * self.height as usize {
                self.tiles[idx as usize] = TileType::Floor;
            }
        }
    }
    pub fn clear_content_index(&mut self) {
        for content in self.content.iter_mut() {
            content.clear();
        }
    }
    pub fn draw(&self, ecs: &World, ctx: &mut Rltk) {
        let mut viewsheds = ecs.write_storage::<Viewshed>();
        let mut players = ecs.write_storage::<Player>();
        let map = ecs.fetch::<Map>();

        for (_player, _viewshed) in (&mut players, &mut viewsheds).join() {
            let mut x = 0;
            let mut y = 0;

            for (idx, tile) in self.tiles.iter().enumerate() {
                if map.revealed_tiles[idx] {
                    let glyph;
                    let mut fg;
                    match tile {
                        TileType::DownStairs => {
                            glyph = rltk::to_cp437('>');
                            fg = RGB::from_f32(0., 1., 1.);
                        }
                        TileType::Floor => {
                            glyph = rltk::to_cp437('.');
                            fg = RGB::from_f32(0.0, 0.5, 0.5);
                        }
                        TileType::Wall => {
                            glyph = rltk::to_cp437('#');
                            fg = RGB::from_f32(0., 1.0, 0.);
                        }
                    }

                    if !map.visible_tiles[idx] {
                        fg = fg.to_greyscale()
                    }
                    ctx.set(x, y, fg, RGB::from_f32(0., 0., 0.), glyph);
                }

                x += 1;
                if x > 79 {
                    x = 0;
                    y += 1;
                }
            }
        }
    }
    pub fn get_room(&self, idx: usize) -> &Rect {
        if let Some(room) = self.rooms.get(idx) {
            room
        } else {
            panic!("No rooms in map")
        }
    }
    pub fn is_exit_valid(&self, x: usize, y: usize) -> bool {
        if x < 1 || x > self.width.sub(1) || y < 1 || y > self.height.sub(1) {
            return false;
        }
        let idx = self.u_xy_idx(x, y);
        !self.blocked[idx]
    }
    pub fn new_map_rooms_and_corridors(new_depth: usize) -> Map {
        let mut map = Map {
            tiles: vec![TileType::Wall; MAPCOUNT],
            rooms: Vec::new(),
            width: MAPWIDTH,
            width_i: MAPWIDTH as i32,
            height: MAPHEIGHT,
            height_i: MAPHEIGHT as i32,
            revealed_tiles: vec![false; MAPCOUNT],
            visible_tiles: vec![false; MAPCOUNT],
            blocked: vec![false; MAPCOUNT],
            content: vec![Vec::new(); MAPCOUNT],
            depth: new_depth,
        };

        const MAX_ROOMS: usize = 30;
        const MIN_SIZE: usize = 6;
        const MAX_SIZE: usize = 10;

        let mut rng = RNG::new();

        for _i in 0..MAX_ROOMS {
            let w = rng.gen_range(MIN_SIZE, MAX_SIZE);
            let h = rng.gen_range(MIN_SIZE, MAX_SIZE);
            let x = rng.roll_dice_u(1, map.width.sub(w as usize + 1)).sub(1);
            let y = rng.roll_dice_u(1, map.height.sub(h as usize + 1)).sub(1);
            let new_room = Rect::new(x, y, w, h);
            let mut ok = true;
            for other_room in map.rooms.iter() {
                if new_room.intersect(other_room) {
                    ok = false
                }
            }
            if ok {
                map.apply_room_to_map(&new_room);

                if !map.rooms.is_empty() {
                    let (new_x, new_y) = new_room.center();
                    let (prev_x, prev_y) = map.rooms[map.rooms.len() - 1].center();
                    if rng.gen_range(0, 2) == 1 {
                        map.apply_horizontal_tunnel(prev_x, new_x, prev_y);
                        map.apply_vertical_tunnel(prev_y, new_y, new_x);
                    } else {
                        map.apply_vertical_tunnel(prev_y, new_y, prev_x);
                        map.apply_horizontal_tunnel(prev_x, new_x, new_y);
                    }
                }

                map.rooms.push(new_room);
            }
        }

        let stairs_position = map.rooms[map.rooms.len() - 1].center();
        let stairs_idx = map.u_xy_idx(stairs_position.0, stairs_position.1);
        map.tiles[stairs_idx] = TileType::DownStairs;

        map
    }
    pub fn populate_blocked(&mut self) {
        for (i, tile) in self.tiles.iter_mut().enumerate() {
            self.blocked[i] = *tile == TileType::Wall;
        }
    }
    pub fn u_xy_idx(&self, x: usize, y: usize) -> usize {
        (y * 80) + x
    }
    pub fn xy_idx(&self, x: i32, y: i32) -> usize {
        (y as usize * 80) + x as usize
    }
}

impl BaseMap for Map {
    fn is_opaque(&self, idx: usize) -> bool {
        self.tiles[idx] == TileType::Wall
    }

    fn get_pathing_distance(&self, idx1: usize, idx2: usize) -> f32 {
        let w = self.width as usize;
        let p1 = Point::new(idx1 % w, idx1 / w);
        let p2 = Point::new(idx2 % w, idx2 / w);
        rltk::DistanceAlg::Pythagoras.distance2d(p1, p2)
    }

    fn get_available_exits(&self, idx: usize) -> Vec<(usize, f32)> {
        let mut exits: Vec<(usize, f32)> = Vec::new();
        let x = idx % self.width;
        let y = idx / self.width;
        let w = self.width as usize;

        // Cardinal directions
        if self.is_exit_valid(x - 1, y) {
            exits.push((idx - 1, 1.0))
        };
        if self.is_exit_valid(x + 1, y) {
            exits.push((idx + 1, 1.0))
        };
        if self.is_exit_valid(x, y - 1) {
            exits.push((idx - w, 1.0))
        };
        if self.is_exit_valid(x, y + 1) {
            exits.push((idx + w, 1.0))
        };

        // Diagonals
        if self.is_exit_valid(x - 1, y - 1) {
            exits.push(((idx - w) - 1, 1.45));
        }
        if self.is_exit_valid(x + 1, y - 1) {
            exits.push(((idx - w) + 1, 1.45));
        }
        if self.is_exit_valid(x - 1, y + 1) {
            exits.push(((idx + w) - 1, 1.45));
        }
        if self.is_exit_valid(x + 1, y + 1) {
            exits.push(((idx + w) + 1, 1.45));
        }

        exits
    }
}

impl Algorithm2D for Map {
    fn dimensions(&self) -> Point {
        Point::new(self.width, self.height)
    }
}
