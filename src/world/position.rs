use specs::{error::NoError, prelude::*, saveload::*};
use std::ops::Sub;

#[derive(Component, ConvertSaveload)]
pub struct Position {
    pub x: usize,
    pub y: usize,
}

impl Position {
    pub fn delta_x(&self, delta: i32) -> i32 {
        if delta < 0 {
            let x = delta.abs() as usize;
            if x < self.x {
                self.x.sub(x) as i32
            } else {
                0
            }
        } else {
            self.x as i32 + delta
        }
    }
    pub fn delta_xu(&self, delta: i32) -> usize {
        if delta < 0 {
            let x = delta.abs() as usize;
            if x < self.x {
                self.x.sub(x)
            } else {
                0
            }
        } else {
            self.x + delta as usize
        }
    }
    pub fn delta_y(&self, delta: i32) -> i32 {
        if delta < 0 {
            let y = delta.abs() as usize;
            if y < self.y {
                self.y.sub(y) as i32
            } else {
                0
            }
        } else {
            self.y as i32 + delta
        }
    }
    pub fn delta_yu(&self, delta: i32) -> usize {
        if delta < 0 {
            let y = delta.abs() as usize;
            if y < self.y {
                self.y.sub(y)
            } else {
                0
            }
        } else {
            self.y + delta as usize
        }
    }
    pub fn set_x(&mut self, x: usize) {
        self.x = x;
    }
    pub fn set_y(&mut self, y: usize) {
        self.y = y;
    }
}
