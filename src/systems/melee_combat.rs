use crate::{
    entities::{CombatStats, Equipped, Name, SuffersDamage, WantsToMelee},
    GameLog,
    items::{DefenseBonus, MeleePowerBonus},
};
use specs::prelude::*;

pub struct MeleeCombatSystem {}

impl<'a> System<'a> for MeleeCombatSystem {
    type SystemData = (
        ReadStorage<'a, CombatStats>,
        ReadStorage<'a, DefenseBonus>,
        Entities<'a>,
        ReadStorage<'a, Equipped>,
        WriteExpect<'a, GameLog>,
        ReadStorage<'a, MeleePowerBonus>,
        ReadStorage<'a, Name>,
        WriteStorage<'a, SuffersDamage>,
        WriteStorage<'a, WantsToMelee>,
    );

    fn run(&mut self, data: Self::SystemData) {
        let (
            combat_stats,
            defense_bonus,
            entities,
            equipped,
            mut log,
            melee_bonus,
            names,
            mut inflict_dmg,
            mut wants_melee) = data;

        for (entity, wants_melee, name, stats) in
            (&entities, &wants_melee, &names, &combat_stats).join()
        {
            if stats.hp > 0 {
                let mut offense_bonus = 0;
                for (_item_entity, power_bonus, equipped_by) in (&entities, &melee_bonus, &equipped).join() {
                    if equipped_by.owner == entity {
                        offense_bonus += power_bonus.power;
                    }
                }

                let target_stats = combat_stats.get(wants_melee.target).unwrap();
                if target_stats.hp > 0 {
                    let target_name = names.get(wants_melee.target).unwrap();

                    let mut defensive_bonus = 0;
                    for (_item_entity, defense_bonus, equipped_by) in (&entities, &defense_bonus, &equipped).join() {
                        if equipped_by.owner == wants_melee.target {
                            defensive_bonus += defense_bonus.defense;
                        }
                    }

                    let damage = i32::max(0, (stats.power + offense_bonus) - (target_stats.defense + defensive_bonus));
                    if damage == 0 {
                        log.entries.insert(
                            0,
                            format!("{} is unable to hurt {}", &name.name, &target_name.name),
                        );
                    } else {
                        log.entries.insert(
                            0,
                            format!(
                                "{} hits {}, for {} hp.",
                                &name.name, &target_name.name, damage
                            ),
                        );
                        inflict_dmg
                            .insert(wants_melee.target, SuffersDamage { damage })
                            .expect("Unable to do damage");
                    }
                }
            }
        }

        wants_melee.clear();
    }
}
