use specs::prelude::*;

use crate::{items::*, *};

pub struct ItemUseSystem {}

impl<'a> System<'a> for ItemUseSystem {
    #[allow(clippy::type_complexity)]
    type SystemData = (
        ReadStorage<'a, AreaOfEffect>,
        ReadStorage<'a, Consumable>,
        WriteStorage<'a, CombatStats>,
        ReadExpect<'a, Entity>,
        ReadStorage<'a, Equippable>,
        WriteStorage<'a, Equipped>,
        WriteExpect<'a, GameLog>,
        ReadStorage<'a, HealsDamage>,
        ReadStorage<'a, InflictsDamage>,
        WriteStorage<'a, InBackpack>,
        Entities<'a>,
        ReadStorage<'a, Name>,
        ReadExpect<'a, Map>,
        WriteStorage<'a, SuffersDamage>,
        WriteStorage<'a, WantsToUseItem>,
    );

    fn run(&mut self, data: Self::SystemData) {
        let (
            aoe,
            consumables,
            mut combat_stats,
            entity,
            equippable,
            mut equipped,
            mut log,
            heals,
            inflict_damage,
            mut in_pack,
            entities,
            name,
            map,
            mut suffer_damage,
            mut wtu_item,
        ) = data;

        for (l_entities, wtuitem) in (&entities, &wtu_item).join() {
            let mut used_item = true;

            let mut targets: Vec<Entity> = Vec::new();
            match wtuitem.target {
                None => {
                    targets.push(*entity);
                }
                Some(target) => {
                    let area_effect = aoe.get(wtuitem.item);
                    match area_effect {
                        None => {
                            // Single target in tile
                            let idx = map.xy_idx(target.x, target.y);
                            for mob in map.content[idx].iter() {
                                targets.push(*mob);
                            }
                        }
                        Some(area_effect) => {
                            // AoE
                            let mut blast_tiles =
                                rltk::field_of_view(target, area_effect.radius as i32, &*map);
                            blast_tiles.retain(|p| {
                                p.x > 0
                                    && p.x < map.width as i32 - 1
                                    && p.y > 0
                                    && p.y < map.height as i32 - 1
                            });
                            for tile_idx in blast_tiles.iter() {
                                let idx = map.xy_idx(tile_idx.x, tile_idx.y);
                                for mob in map.content[idx].iter() {
                                    targets.push(*mob);
                                }
                            }
                        }
                    }
                }
            }

            if let Some(item_equippable) = equippable.get(wtuitem.item) {
                let target_slot = item_equippable.slot;
                let target = targets[0];

                // Remove any items the target has in the item's slot
                let mut to_unequip: Vec<Entity> = Vec::new();
                for (item_entity, already_equipped, name) in (&entities, &equipped, &name).join() {
                    if already_equipped.owner == target && already_equipped.slot == target_slot {
                        to_unequip.push(item_entity);
                        if target == *entity {
                            log.entries.insert(0, format!("You unequip {}.", name.name));
                        }
                    }
                }
                for item in to_unequip.iter() {
                    equipped.remove(*item);
                    in_pack
                        .insert(*item, InBackpack { owner: target })
                        .expect("Unable to insert backpack entry");
                }

                // Wield the item
                equipped
                    .insert(
                        wtuitem.item,
                        Equipped {
                            owner: target,
                            slot: target_slot,
                        },
                    )
                    .expect("Unable to insert equipped component");
                in_pack.remove(wtuitem.item);
                if target == *entity {
                    log.entries.insert(
                        0,
                        format!("You equip {}.", name.get(wtuitem.item).unwrap().name),
                    );
                }
            }

            let item_heals = heals.get(wtuitem.item);
            match item_heals {
                None => {}
                Some(heal) => {
                    used_item = false;
                    for target in targets.iter() {
                        let stats = combat_stats.get_mut(*target);
                        if let Some(stats) = stats {
                            stats.hp = i32::min(stats.max_hp, stats.hp + heal.amount);
                            if l_entities == *entity {
                                log.entries.insert(
                                    0,
                                    format!(
                                        "you use the {}, healing {} hp.",
                                        name.get(wtuitem.item).unwrap().name,
                                        heal.amount
                                    ),
                                );
                            }
                            used_item = true;
                        }
                    }
                }
            }

            let dmg_health = inflict_damage.get(wtuitem.item);
            match dmg_health {
                None => {}
                Some(damage) => {
                    let target_point = wtuitem.target.unwrap();
                    let idx = map.xy_idx(target_point.x, target_point.y);
                    used_item = false;
                    for mob in map.content[idx].iter() {
                        suffer_damage
                            .insert(
                                *mob,
                                SuffersDamage {
                                    damage: damage.damage,
                                },
                            )
                            .expect("Unable to insert");
                        if l_entities == *entity {
                            let mob_name = name.get(*mob).unwrap();
                            let item_name = name.get(wtuitem.item).unwrap();
                            log.entries.insert(
                                0,
                                format!(
                                    "You use {} on {}, inflicting {} hp.",
                                    item_name.name, mob_name.name, damage.damage
                                ),
                            );
                        }

                        used_item = true;
                    }
                }
            }

            if used_item {
                let consumable = consumables.get(wtuitem.item);
                match consumable {
                    None => {}
                    Some(_) => entities
                        .delete(wtuitem.item)
                        .expect("Deletion of item failed."),
                }
            }
        }

        wtu_item.clear();
    }
}
