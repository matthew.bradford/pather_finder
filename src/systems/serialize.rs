use std::sync::Arc;

use serde::{Deserialize, Serialize};
use specs::{error::NoError, prelude::*, saveload::*};
use specs_derive::Component;

use crate::entities::mob::*;
use crate::entities::player::*;
use crate::entities::*;
use crate::items::*;
use crate::world::map::Map;
use crate::world::position::Position;
use crate::Renderable;
use crate::SMSerialize;
use mycelium_command::prelude::*;
use mycelium_lib::Mycelium;

#[derive(Serialize, Deserialize, Clone)]
pub struct EquippedData<M>(M, EquipmentSlot);

impl<M: Marker + Serialize> ConvertSaveload<M> for Equipped
where
    for<'de> M: Deserialize<'de>,
{
    type Data = EquippedData<M>;
    type Error = NoError;

    fn convert_into<F>(&self, mut ids: F) -> Result<Self::Data, Self::Error>
    where
        F: FnMut(Entity) -> Option<M>,
    {
        let marker = ids(self.owner).unwrap();
        Ok(EquippedData(marker, self.slot))
    }

    fn convert_from<F>(data: Self::Data, mut ids: F) -> Result<Self, Self::Error>
    where
        F: FnMut(M) -> Option<Entity>,
    {
        let entity = ids(data.0).unwrap();
        Ok(Equipped {
            owner: entity,
            slot: data.1,
        })
    }
}

#[derive(Clone, Component, ConvertSaveload, Debug)]
pub struct SerializationHelper {
    pub map: crate::world::map::Map,
}

macro_rules! serialize_individually {
    ($ecs:expr, $ser:expr, $data:expr, $( $type:ty),*) => {
        $(
        SerializeComponents::<NoError, SimpleMarker<SMSerialize>>::serialize(
            &( $ecs.read_storage::<$type>(), ),
            &$data.0,
            &$data.1,
            &mut $ser,
        )
        .unwrap();
        )*
    };
}

pub fn save_game(ecs: &mut World) {
    let mapcopy = ecs.get_mut::<crate::world::map::Map>().unwrap().clone();
    let save_helper = ecs
        .create_entity()
        .with(SerializationHelper { map: mapcopy })
        .marked::<SimpleMarker<SMSerialize>>()
        .build();

    {
        let data = (
            ecs.entities(),
            ecs.read_storage::<SimpleMarker<SMSerialize>>(),
        );
        let mut ser = ron::ser::Serializer::new(None, false);
        serialize_individually!(
            ecs,
            ser,
            data,
            AreaOfEffect,
            BlocksTile,
            CombatStats,
            Consumable,
            DefenseBonus,
            Equipped,
            Equippable,
            HealsDamage,
            InBackpack,
            InflictsDamage,
            Item,
            Map,
            MeleePowerBonus,
            Monster,
            Name,
            Player,
            Position,
            Ranged,
            Renderable,
            SerializationHelper,
            SuffersDamage,
            Viewshed,
            WantsToDrink,
            WantsToDropItem,
            WantsToMelee,
            WantsToPickupItem,
            WantsToRemoveItem,
            WantsToUseItem
        );

        let db = ecs.fetch_mut::<Arc<Mycelium>>();
        let cmd = Command::new()
            .with_tag(crate::GAME_INDEX_NAME)
            .with_action(Action::Insert(Some(
                ser.into_output_string().as_bytes().to_vec(),
            )))
            .with_what(What::Index(crate::GAME_INDEX_NAME.to_string()));

        match db.execute_command(cmd) {
            Ok(_) => println!("Game Saved"),
            Err(e) => println!("Error Saving: {:?}", e),
        }
    }
    ecs.delete_entity(save_helper).expect("Crash on cleanup");
}
