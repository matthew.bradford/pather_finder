use std::sync::Arc;

use mycelium_command::prelude::*;
use mycelium_lib::prelude::*;

use crate::entities::{mob::Monster, player::Player, *};
use crate::items::*;
use crate::systems::serialize::SerializationHelper;
use crate::world::map::Map;
use crate::world::position::*;
use crate::Renderable;
use crate::SMSerialize;
use specs::{
    error::NoError,
    prelude::*,
    saveload::{DeserializeComponents, SimpleMarker, SimpleMarkerAllocator},
};

macro_rules! deserialize_individually {
    ($ecs:expr, $de:expr, $data:expr, $( $type:ty),*) => {
        $(
        DeserializeComponents::<NoError, _>::deserialize(
            &mut ( &mut $ecs.write_storage::<$type>(), ),
            &mut $data.0, // entities
            &mut $data.1, // marker
            &mut $data.2, // allocater
            &mut $de,
        )
        .unwrap();
        )*
    };
}

pub fn load_game(ecs: &mut World) {
    {
        let mut to_delete = vec![];
        for e in ecs.entities().join() {
            to_delete.push(e);
        }
        for del in to_delete.iter() {
            ecs.delete_entity(*del).expect("Deletion Failed");
        }
    }

    let mut b_vec = vec![];
    {
        let db = ecs.fetch_mut::<Arc<Mycelium>>();
        let cmd = Command::new()
            .with_tag(crate::GAME_INDEX_NAME)
            .with_action(Action::Select)
            .with_what(What::Index(crate::GAME_INDEX_NAME.to_string()));

        match db.execute_command(cmd) {
            Ok((list, _msg)) => {
                if list.is_some() {
                    let unwrap = list.unwrap();
                    let (_tag, _id, item) = unwrap.first().unwrap();
                    if let Some(item) = item {
                        b_vec = item.to_vec();
                    }
                }
            }
            Err(e) => println!("Error Saving: {:?}", e),
        }
    }

    let mut de = ron::de::Deserializer::from_bytes(&b_vec).unwrap();
    {
        let mut data = (
            &mut ecs.entities(),
            &mut ecs.write_storage::<SimpleMarker<SMSerialize>>(),
            &mut ecs.write_resource::<SimpleMarkerAllocator<SMSerialize>>(),
        );

        deserialize_individually!(
            ecs,
            de,
            data,
            AreaOfEffect,
            BlocksTile,
            CombatStats,
            Consumable,
            DefenseBonus,
            Equipped,
            Equippable,
            HealsDamage,
            InBackpack,
            InflictsDamage,
            Item,
            Map,
            MeleePowerBonus,
            Monster,
            Name,
            Player,
            Position,
            Ranged,
            Renderable,
            SerializationHelper,
            SuffersDamage,
            Viewshed,
            WantsToDrink,
            WantsToDropItem,
            WantsToMelee,
            WantsToPickupItem,
            WantsToRemoveItem,
            WantsToUseItem
        );
    }

    let mut deleteme: Option<Entity> = None;
    {
        let entities = ecs.entities();
        let helper = ecs.read_storage::<SerializationHelper>();
        let player = ecs.read_storage::<Player>();
        let position = ecs.read_storage::<Position>();

        for (e, h) in (&entities, &helper).join() {
            let mut worldmap = ecs.write_resource::<crate::world::map::Map>();
            *worldmap = h.map.clone();
            worldmap.content = vec![Vec::new(); crate::world::map::MAPCOUNT];
            deleteme = Some(e);
        }
        for (e, _p, pos) in (&entities, &player, &position).join() {
            let mut ppos = ecs.write_resource::<rltk::Point>();
            *ppos = rltk::Point::new(pos.x, pos.y);
            let mut player_resource = ecs.write_resource::<Entity>();
            *player_resource = e;
        }
    }
    ecs.delete_entity(deleteme.unwrap())
        .expect("Unable to delete helper");
}
