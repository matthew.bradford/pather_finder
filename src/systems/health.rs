use crate::{
    entities::{player::Player, CombatStats, Name, SuffersDamage},
    GameLog,
    RunState,
};
use rltk::console;
use specs::prelude::*;

pub struct HealthSystem {}

impl<'a> System<'a> for HealthSystem {
    type SystemData = (
        WriteStorage<'a, CombatStats>,
        WriteStorage<'a, SuffersDamage>,
    );

    fn run(&mut self, data: Self::SystemData) {
        let (mut stats, mut dmg) = data;

        for (mut stats, dmg) in (&mut stats, &dmg).join() {
            stats.hp -= dmg.damage as i32;
        }

        dmg.clear();
    }
}

pub fn bring_out_the_dead(ecs: &mut World) {
    let mut dead: Vec<Entity> = Vec::new();
    // Using a scope to make the borrow checker happy
    {
        let combat_stats = ecs.read_storage::<CombatStats>();
        let mut log = ecs.write_resource::<GameLog>();
        let players = ecs.read_storage::<Player>();
        let entities = ecs.entities();
        let names = ecs.read_storage::<Name>();
        for (entity, stats) in (&entities, &combat_stats).join() {
            if stats.hp < 1 {
                let player = players.get(entity);
                match player {
                    None => {
                        let victim_name = names.get(entity);
                        if let Some(victim_name) = victim_name {
                            log.entries
                                .insert(0, format!("{} is dead", &victim_name.name));
                        }
                        dead.push(entity)
                    }
                    Some(_) => {
                        let mut runstate = ecs.write_resource::<RunState>();
                        *runstate = RunState::GameOver;
                    }
                }
            }
        }
    }

    for victim in dead {
        ecs.delete_entity(victim).expect("Unable to delete");
    }
}
