use rltk::{Point, RGB};
use specs::{
    error::NoError,
    prelude::*,
    saveload::{ConvertSaveload, MarkedBuilder, Marker, SimpleMarker},
};

use crate::{
    entities::WantsToPickupItem, world::position::Position, GameLog, Name, Renderable, SMSerialize,
};

#[derive(Clone, Component, ConvertSaveload, Debug)]
pub struct AreaOfEffect {
    pub radius: usize,
}

#[derive(Clone, Component, serde::Deserialize, serde::Serialize, Debug)]
pub struct Consumable {}

#[derive(Component, ConvertSaveload, Clone)]
pub struct DefenseBonus {
    pub defense: i32,
}

#[derive(PartialEq, Copy, Clone, Serialize, Deserialize)]
pub enum EquipmentSlot {
    Melee,
    Shield,
}

#[derive(Component, Serialize, Deserialize, Clone)]
pub struct Equippable {
    pub slot: EquipmentSlot,
}

#[derive(Clone, Component, ConvertSaveload, Debug)]
pub struct HealsDamage {
    pub amount: i32,
}

#[derive(Clone, Component, ConvertSaveload, Debug)]
pub struct InflictsDamage {
    pub damage: i32,
}

#[derive(Clone, Component, serde::Deserialize, serde::Serialize, Debug)]
pub struct Item {}

#[derive(Component, ConvertSaveload, Clone)]
pub struct MeleePowerBonus {
    pub power: i32,
}

#[derive(Clone, Component, ConvertSaveload, Debug)]
pub struct Ranged {
    pub range: i32,
}

pub fn dagger(ecs: &mut World, x: usize, y: usize) {
    ecs.create_entity()
        .with(Equippable {
            slot: EquipmentSlot::Melee,
        })
        .with(Item {})
        .with(MeleePowerBonus { power: 12 })
        .with(Name {
            name: "Dagger".to_string(),
        })
        .with(Position { x, y })
        .with(Renderable {
            glyph: rltk::to_cp437('/'),
            fg: RGB::named(rltk::CYAN),
            bg: RGB::named(rltk::BLACK),
            render_order: 2,
        })
        .marked::<SimpleMarker<SMSerialize>>()
        .build();
}
pub fn fireball_scroll(ecs: &mut World, x: usize, y: usize) {
    ecs.create_entity()
        .with(Position { x: x, y: y })
        .with(Renderable {
            glyph: rltk::to_cp437('f'),
            fg: RGB::named(rltk::RED),
            bg: RGB::named(rltk::BLACK),
            render_order: 2,
        })
        .with(Name {
            name: String::from("Fireball Scroll"),
        })
        .with(Item {})
        .with(Consumable {})
        .with(Ranged { range: 8 })
        .with(InflictsDamage { damage: 20 })
        .with(AreaOfEffect { radius: 5 })
        .marked::<SimpleMarker<SMSerialize>>()
        .build();
}
pub fn get_item(ecs: &mut World) {
    let player_pos = ecs.fetch::<Point>();
    let player_entity = ecs.fetch::<Entity>();
    let entities = ecs.entities();
    let items = ecs.read_storage::<Item>();
    let positions = ecs.read_storage::<Position>();
    let mut gamelog = ecs.fetch_mut::<GameLog>();

    let mut target_item: Option<Entity> = None;
    for (item_entity, _item, position) in (&entities, &items, &positions).join() {
        if position.x == player_pos.x as usize && position.y == player_pos.y as usize {
            target_item = Some(item_entity);
        }
    }

    match target_item {
        None => gamelog
            .entries
            .insert(0, "There is nothing here to pick up.".to_string()),
        Some(item) => {
            let mut pickup = ecs.write_storage::<WantsToPickupItem>();
            pickup
                .insert(
                    *player_entity,
                    WantsToPickupItem {
                        collected_by: *player_entity,
                        item,
                    },
                )
                .expect("Unable to insert want to pickup");
        }
    }
}
pub fn health_potion(ecs: &mut World, x: usize, y: usize) {
    ecs.create_entity()
        .with(Position { x: x, y: y })
        .with(Renderable {
            glyph: rltk::to_cp437('i'),
            fg: RGB::named(rltk::MAGENTA),
            bg: RGB::named(rltk::BLACK),
            render_order: 2,
        })
        .with(Name {
            name: String::from("Health Potion"),
        })
        .with(Item {})
        .with(Consumable {})
        .with(HealsDamage { amount: 12 })
        .marked::<SimpleMarker<SMSerialize>>()
        .build();
}
pub fn magic_missle_scroll(ecs: &mut World, x: usize, y: usize) {
    ecs.create_entity()
        .with(Position { x: x, y: y })
        .with(Renderable {
            glyph: rltk::to_cp437('%'),
            fg: RGB::named(rltk::CYAN),
            bg: RGB::named(rltk::BLACK),
            render_order: 2,
        })
        .with(Name {
            name: String::from("Magic Missle Scroll"),
        })
        .with(Item {})
        .with(Consumable {})
        .with(Ranged { range: 5 })
        .with(InflictsDamage { damage: 8 })
        .marked::<SimpleMarker<SMSerialize>>()
        .build();
}
pub fn shield(ecs: &mut World, x: usize, y: usize) {
    ecs.create_entity()
        .with(DefenseBonus { defense: 8 })
        .with(Equippable {
            slot: EquipmentSlot::Shield,
        })
        .with(Item {})
        .with(Name {
            name: "Shield".to_string(),
        })
        .with(Position { x, y })
        .with(Renderable {
            glyph: rltk::to_cp437('('),
            fg: RGB::named(rltk::CYAN),
            bg: RGB::named(rltk::BLACK),
            render_order: 2,
        })        
        .marked::<SimpleMarker<SMSerialize>>()
        .build();
}
