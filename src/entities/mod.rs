use crate::items::EquipmentSlot;
use specs::{
    error::NoError,
    prelude::*,
    saveload::{ConvertSaveload, Marker},
};

pub mod mob;
pub mod player;
pub mod spawner;

#[derive(Clone, Component, Debug, serde::Deserialize, serde::Serialize)]
pub struct BlocksTile {}

#[derive(Clone, Component, ConvertSaveload, Debug)]
pub struct CombatStats {
    pub max_hp: i32,
    pub hp: i32,
    pub defense: i32,
    pub power: i32,
}

#[derive(Clone, Component)]
pub struct Equipped {
    pub owner: Entity,
    pub slot: EquipmentSlot,
}

#[derive(Component, ConvertSaveload, Debug, Clone)]
pub struct InBackpack {
    pub owner: Entity,
}

#[derive(Component, Clone, ConvertSaveload, Debug)]
pub struct Name {
    pub name: String,
}

#[derive(Component, ConvertSaveload, Debug)]
pub struct SuffersDamage {
    pub damage: i32,
}

#[derive(Component, ConvertSaveload)]
pub struct Viewshed {
    pub visible_tiles: Vec<rltk::Point>,
    pub range: usize,
    pub dirty: bool,
}

#[derive(Component, ConvertSaveload, Debug)]
pub struct WantsToDrink {
    pub potion: Entity,
}

#[derive(Component, ConvertSaveload, Debug)]
pub struct WantsToMelee {
    pub target: Entity,
}

#[derive(Component, ConvertSaveload, Debug, Clone)]
pub struct WantsToDropItem {
    pub item: Entity,
}

#[derive(Component, ConvertSaveload, Debug, Clone)]
pub struct WantsToPickupItem {
    pub collected_by: Entity,
    pub item: Entity,
}

#[derive(Component, Debug, ConvertSaveload, Clone)]
pub struct WantsToRemoveItem {
    pub item : Entity
}

#[derive(Component, ConvertSaveload, Debug, Clone)]
pub struct WantsToUseItem {
    pub item: Entity,
    pub target: Option<rltk::Point>,
}
