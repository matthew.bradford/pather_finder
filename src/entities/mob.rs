use crate::{
    entities::{BlocksTile, CombatStats, Name},
    world::position::Position,
    Renderable, SMSerialize, Viewshed,
};
use rltk::RGB;
use specs::prelude::*;
use specs::saveload::{MarkedBuilder, SimpleMarker};

pub fn goblin(ecs: &mut World, x: usize, y: usize) {
    monster(ecs, x, y, rltk::to_cp437('g'), "Goblin");
}

#[derive(Clone, Component, Debug, Deserialize, Serialize)]
pub struct Monster {}

fn monster<S: ToString>(ecs: &mut World, x: usize, y: usize, glyph: u8, name: S) {
    ecs.create_entity()
        .with(Position { x, y })
        .with(Renderable {
            glyph,
            fg: RGB::named(rltk::RED),
            bg: RGB::named(rltk::BLACK),
            render_order: 1,
        })
        .with(Viewshed {
            visible_tiles: Vec::new(),
            range: 8,
            dirty: true,
        })
        .with(Monster {})
        .with(Name {
            name: name.to_string(),
        })
        .with(BlocksTile {})
        .with(CombatStats {
            max_hp: 16,
            hp: 16,
            defense: 1,
            power: 4,
        })
        .marked::<SimpleMarker<SMSerialize>>()
        .build();
}

pub fn orc(ecs: &mut World, x: usize, y: usize) {
    monster(ecs, x, y, rltk::to_cp437('o'), "Orc");
}
