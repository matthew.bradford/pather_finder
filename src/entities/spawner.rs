use std::collections::HashMap;
use std::convert::TryInto;
use std::ops::Sub;

use rltk::RGB;
use specs::{
    prelude::*,
    saveload::{MarkedBuilder, SimpleMarker},
};

use crate::{
    entities::{mob::*, player::Player, *},
    items::*,
    random::{table::RandomTable, RNG},
    world::{map::*, position::Position, rect::Rect},
    Renderable, SMSerialize,
};

pub const MAX_MONSTERS: usize = 3;
/// Spawns the player and returns his/her entity object.
pub fn player(ecs: &mut World, player_x: usize, player_y: usize) -> Entity {
    ecs.create_entity()
        .with(Position {
            x: player_x,
            y: player_y,
        })
        .with(Renderable {
            glyph: rltk::to_cp437('@'),
            fg: RGB::named(rltk::YELLOW),
            bg: RGB::named(rltk::BLACK),
            render_order: 0,
        })
        .with(Player {})
        .with(Viewshed {
            visible_tiles: Vec::new(),
            range: 8,
            dirty: true,
        })
        .with(Name {
            name: "Player".to_string(),
        })
        .with(CombatStats {
            max_hp: 65,
            hp: 65,
            defense: 2,
            power: 8,
        })
        .marked::<SimpleMarker<SMSerialize>>()
        .build()
}
pub fn spawn_room(ecs: &mut World, room: &Rect, map_depth: usize) {
    let spawn_table = room_table(map_depth);
    let mut spawn_points: HashMap<usize, String> = HashMap::new();

    {
        let mut rng = ecs.write_resource::<RNG>();
        let num_spawns = rng.roll_dice_u(1, MAX_MONSTERS + 3) + (map_depth.sub(1));

        for _i in 0..num_spawns {
            let mut added = false;
            let mut tries = 0;
            while !added && tries < 20 {
                let x = room.x1 + rng.roll_dice_u(1, room.x2.sub(room.x1));
                let y = room.y1 + rng.roll_dice_u(1, room.y2.sub(room.y1));
                let idx = (y * MAPWIDTH) + x;
                if !spawn_points.contains_key(&idx) {
                    spawn_points.insert(idx, spawn_table.roll(&mut rng));
                    added = true;
                } else {
                    tries += 1;
                }
            }
        }
    }

    // Actually spawn the monsters
    for spawn in spawn_points.iter() {
        let x = *spawn.0 % MAPWIDTH;
        let y = *spawn.0 / MAPWIDTH;

        match spawn.1.as_ref() {
            "Goblin" => goblin(ecs, x.try_into().unwrap(), y.try_into().unwrap()),
            "Orc" => orc(ecs, x.try_into().unwrap(), y.try_into().unwrap()),
            "Health Potion" => health_potion(ecs, x, y),
            "Fireball Scroll" => fireball_scroll(ecs, x, y),
            "Magic Missile Scroll" => magic_missle_scroll(ecs, x, y),
            "Dagger" => dagger(ecs, x, y),
            "Shield" => shield(ecs, x, y),
            _ => {}
        }
    }
}
fn room_table(map_depth: usize) -> RandomTable {
    RandomTable::new()
        .add("Goblin", 10)
        .add("orc", 1 + map_depth)
        .add("Health Potion", 7 + map_depth)
        .add("Fireball Scroll", 2 + map_depth)
        .add("Magic Missile Scroll", 10 + map_depth)
        .add("Dagger", 3)
        .add("Shield", 3)
}
